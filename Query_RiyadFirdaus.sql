CREATE DATABASE coding_id;
USE coding_id;

CREATE TABLE users(pk_user_id INT PRIMARY KEY IDENTITY(1,1), name VARCHAR(255));
CREATE TABLE tasks(pk_task_id INT PRIMARY KEY IDENTITY(1,1), task_detail VARCHAR(MAX), fk_user_id INT);