﻿using CodingID.Data;
using CodingID.Models;
using CodingID.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
#nullable enable

namespace CodingID.Controllers
{
    [Route("api/")]
    [ApiController]

    public class CodingIDController : ControllerBase
    {
        //Buat objek dari class UserData dan TaskData
        private readonly UserData _userData;
        private readonly TaskData _taskData;

        //Constructor
        public CodingIDController(UserData userData, TaskData taskData)
        {
            _userData = userData;
            _taskData = taskData;
        }

        //Route Attribute 
        [HttpGet("GetUserWithTask")]

        //IActionResult merupakan return type dengan hasil return yaitu {ActionResult}
        public IActionResult GetAll(string? name)
        {

            try
            {
                List<User> users = new List<User>();
                List<UserWithTaskDto> output = new List<UserWithTaskDto>();
                if (name == null)
                {
                    users = _userData.GetAll();
                }
                else users = _userData.GetUserByName(name);

                foreach (User user in users)
                {
                    List<TaskOutputDto> taskOutputDto = _taskData.GetByUserId(user.pk_user_id);

                    output.Add(new UserWithTaskDto
                    {
                        tasks = taskOutputDto,
                        pk_user_id = user.pk_user_id,
                        name = user.name
                    });
                }
                return Ok(output);
            }
            catch (Exception ex)
            {
                //Jika gagal return status code dan messagenya
                return StatusCode(500, ex.Message);
            }

        }


        [HttpPost("AddUserWithTask")]
        public IActionResult Post([FromBody] AddDto addDto)
        {
            try
            {
                User user = new User
                {
                    name = addDto.name
                };

                int insertedId = _userData.Insert(user);

                if (insertedId > 0)
                {
                    foreach (TaskInputDto taskInputDto in addDto.tasks)
                    {
                        UserTask task = new UserTask
                        {
                            task_detail = taskInputDto.task_detail,
                            fk_user_id = insertedId
                        };
                        _taskData.Insert(task);
                    }
                    return StatusCode(201, "Data Succesfully Inserted");
                }
                else
                {
                    return StatusCode(500, "Data Not Inserted");
                }

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }


        }
    }
}
