﻿namespace CodingID.Models
{
    public class UserTask
    {
        public int pk_task_id { get; set; }
        public string task_detail { get; set; } = string.Empty;
        public int fk_user_id { get; set; }

    }
}
