﻿using System.Data.SqlClient;
using System.Diagnostics.Eventing.Reader;
using CodingID.Models;
using static System.Reflection.Metadata.BlobBuilder;

namespace CodingID.Data
{
    public class UserData
    {
        // Define connectionString berisi parameter server yang kita buat
        private readonly string ConnectionString = "Data Source=DESKTOP-1RU3JR8\\SQLEXPRESS;Database=coding_id;Integrated Security=True";


        //Define fungsi GetAll() yang mereturn List<User>
        public List<User> GetAll()
        {
            List<User> users = new List<User>();

            string query = "SELECT * FROM users";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                users.Add(new User
                                {
                                    pk_user_id = Convert.ToInt32(reader["pk_user_id"]),
                                    name = reader["name"].ToString() ?? string.Empty
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return users;
        }

        public List<User> GetUserByName(string name)
        {
            List<User> users = new List<User>();

            string query = $"SELECT * FROM users WHERE name = '{name}'";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                users.Add(new User
                                {
                                    pk_user_id = Convert.ToInt32(reader["pk_user_id"]),
                                    name = reader["name"].ToString() ?? string.Empty
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return users;
        }

        public int Insert(User user)
        {
            bool result = false;
            int lastValue = 0;
            string query = $"INSERT INTO users (name) VALUES ('{user.name}')";
            string query2 = $"SELECT IDENT_CURRENT ('users')";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();
                        result = command.ExecuteNonQuery() > 0 ? true : false;
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
                using (SqlCommand command = new SqlCommand(query2, connection))
                {
                    try
                    {
                        connection.Open();
                        lastValue = Convert.ToInt32(command.ExecuteScalar());
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return lastValue;
        }
    }
}
