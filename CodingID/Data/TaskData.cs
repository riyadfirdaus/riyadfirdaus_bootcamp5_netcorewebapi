﻿using CodingID.DTOs;
using CodingID.Models;
using Microsoft.AspNetCore.Identity;
using System.Data.SqlClient;

namespace CodingID.Data
{
    public class TaskData
    {
        private readonly string ConnectionString = "Data Source=DESKTOP-1RU3JR8\\SQLEXPRESS;Database=coding_id;Integrated Security=True";
        public List<UserTask> GetAll()
        {
            List<UserTask> tasks = new List<UserTask>();

            string query = "SELECT * FROM tasks";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    try
                    {
                        command.Connection = connection;
                        command.CommandText = query;

                        command.Parameters.Clear();
                        connection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                tasks.Add(new UserTask
                                {
                                    pk_task_id = Convert.ToInt32(reader["pk_task_id"]),
                                    task_detail = reader["name"].ToString() ?? string.Empty
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return tasks;
        }
        public List<TaskOutputDto> GetByUserId(int Id)
        {
            List<TaskOutputDto> taskOutputDto = new List<TaskOutputDto>();

            string query = $"SELECT * FROM tasks WHERE fk_user_id = @id";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    try
                    {
                        command.Connection = connection;
                        command.CommandText = query;

                        command.Parameters.Clear();

                        command.Parameters.AddWithValue("@id", Id);
                        connection.Open();

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                taskOutputDto.Add(new TaskOutputDto
                                {
                                    pk_task_id = Convert.ToInt32(reader["pk_task_id"]),
                                    task_detail = reader["task_detail"].ToString() ?? string.Empty
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }

                }
            }

            return taskOutputDto;
        }


        public bool Insert(UserTask task)
        {
            bool result = false;
            string query = $"INSERT INTO tasks (task_detail, fk_user_id) VALUES ('{task.task_detail}','{task.fk_user_id}')";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    try
                    {
                        connection.Open();
                        result = command.ExecuteNonQuery() > 0 ? true : false;
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return result;
        }
    }

}

