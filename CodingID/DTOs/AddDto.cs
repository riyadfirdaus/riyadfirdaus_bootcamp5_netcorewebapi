﻿using CodingID.Models;

namespace CodingID.DTOs
{
    public class AddDto
    {
        public string name { get; set; } = string.Empty;
        public List<TaskInputDto> tasks { get; set; } = new List<TaskInputDto>();
    }
}
