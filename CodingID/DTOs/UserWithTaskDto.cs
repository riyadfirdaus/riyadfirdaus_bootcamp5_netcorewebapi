﻿using CodingID.Models;

namespace CodingID.DTOs
{

    public class UserWithTaskDto
    {
        public List<TaskOutputDto> tasks { get; set; } = new List<TaskOutputDto>();
        public int pk_user_id { get; set; }
        public string name { get; set; } = string.Empty;

    }
}
